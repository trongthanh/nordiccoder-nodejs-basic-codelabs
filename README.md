# NordicCoder Node.js Basic - Week 4 Codelabs

## HOWTO:

- Use **Robo 3T** or **MongoDB shell** to `insertMany` users data in `users.json` to a
  database named `nordic` in a collection named `users`
- Use express-generator to generate an Express app with `hbs` as View template engine

  ```sh
  express --view=hbs .
  ```

- Now search for the comment labels in this repo: **STEP#** with **#** is order number starting from 1 to follow the implementation steps
  - There are **10 steps** in total
- Search for **STEP OPTIONAL** for additional tasks that are optional for this codelab

## REST API overview

| Route          | Method | Description                            |
| -------------- | ------ | -------------------------------------- |
| /api/users     | GET    | Get all the users.                     |
| /api/users     | POST   | Create a new user.                     |
| /api/users/:id | GET    | Get a single user.                     |
| /api/users/:id | PATCH  | Update a user partially with new info. |
| /api/users/:id | PUT    | Replace a user entirely with new info. |
| /api/users/:id | DELETE | Delete a user.                         |

## TESTING:

Use **Postman** to test our API during codelab. Below are data for Postman testing what we implement
in this codelab:

### Step 3: Test the /api info

- Method: GET
- URL: http://localhost:3000/api

### Step 6: Test get all users

- Method: GET
- URL: http://localhost:3000/api/users

### Step 7: Test get one user

- Method: GET
- URL: http://localhost:3000/api/users/:id
- Id: Use one of the `_id` from results of Step 6

### Step 8: Test create one new user

- Method: POST
- URL: http://localhost:3000/api/users
- Headers: `Content-Type: application/json`
- Body (raw):
  ```json
  {
    "avatar": "https://api.adorable.io/avatar/yl",
    "firstName": "Yến",
    "lastName": "Lê",
    "dob": "1990-01-01T13:06:35.216Z",
    "gender": "female",
    "country": "vietnam",
    "phoneNumber": "12345678",
    "zipcode": "700000",
    "username": "yenle",
    "email": "yen.le@nordiccoder.com",
    "emailVerified": true,
    "role": "admin"
  }
  ```
- Notes:
  - Use the Schema with validation in `models/User.js`
  - Try posting invalid values to see if Moongse return errors to response JSON

### Step 9: Test update one existing user

- Method: PATCH
- URL: http://localhost:3000/api/users/:id
- Id: Use one of the `_id` from results of Step 6
- Headers: `Content-Type: application/json`
- Body (raw): try posting a partial user detail and see the updated response

### Step 10: Test delete one existing user

- Method: DELETE
- URL: http://localhost:3000/api/users/:id
- Id: Use one of the `_id` from results of Step 6
